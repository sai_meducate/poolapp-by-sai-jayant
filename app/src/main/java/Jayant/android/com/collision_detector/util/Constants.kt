package Jayant.android.com.collision_detector.util

/**
 * Created by SaiJayant's Macbook on 06,July,2021
 */
object Constants {

    val RADIUS = 70
    val TOTAL_SHAPES = 3
    val ACTION_DELETE = 14
    val ACTION_TRANSFORM = 12
    val ACTION_CREATE = -1
}
