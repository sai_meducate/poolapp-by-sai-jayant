package Jayant.android.com.collision_detector.presenter

import android.content.Context
import Jayant.android.com.collision_detector.interactor.ShapesInteractor
import Jayant.android.com.collision_detector.model.Shape.Type
import Jayant.android.com.collision_detector.view.CustomCanvas

/**
 * Created by SaiJayant's Macbook on 06,July,2021
 */
class CanvasPresenter(private val canvas: CustomCanvas, private val mContext: Context) {

    /**
     * Respons to click and long press events on canvas
     */

    init {
        initializeUIComponents(canvas, mContext)
    }

    private fun initializeUIComponents(canvas: CustomCanvas, mContext: Context) {
        ShapesInteractor.instance.canvas = canvas
        ShapesInteractor.instance.setContext(mContext)
    }


    fun setMaxX(maxX: Int) {
        ShapesInteractor.instance.maxX = maxX
    }

    fun setMaxY(maxY: Int) {
        ShapesInteractor.instance.maxY = maxY
    }

    fun addShapeRandom(type: Type) {
        ShapesInteractor.instance.addShapeRandom(type)
    }

    fun undo() {
        ShapesInteractor.instance.undo()
    }

}
