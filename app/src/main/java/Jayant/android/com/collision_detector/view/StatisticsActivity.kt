package Jayant.android.com.collision_detector.view

import Jayant.android.com.collision_detector.R
import Jayant.android.com.collision_detector.adapter.RecyclerViewEmptyObserver
import Jayant.android.com.collision_detector.model.Shape
import Jayant.android.com.collision_detector.presenter.StatisticsPresenter
import Jayant.android.com.collision_detector.util.ToastHelper.makeText
import Jayant.android.com.collision_detector.view.StatsAdapter.OnItemClicked
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

/**
 * Created by SaiJayant's Macbook on 06,July,2021
 */
class StatisticsActivity : AppCompatActivity() {
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: StatsAdapter? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var statsEmptyView: TextView? = null
    var statisticsPresenter: StatisticsPresenter? = null
    private var myDataset: HashMap<Shape.Type, Int>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistic)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        mRecyclerView = findViewById<View>(R.id.stats_recycler_view) as RecyclerView
        statsEmptyView = findViewById<View>(R.id.emptyView) as TextView
        statisticsPresenter = StatisticsPresenter()
        setupStatsListView()
    }

    /*
    Setup the list view to show statistics per shape
     */
    private fun setupStatsListView() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView!!.setHasFixedSize(true)

        // use a linear layout manager
        mLayoutManager = LinearLayoutManager(this)
        mRecyclerView!!.layoutManager = mLayoutManager
        myDataset = statisticsPresenter!!.countByGroup as HashMap<Shape.Type, Int>
        statsEmptyView!!.visibility = View.GONE
        mAdapter = StatsAdapter(myDataset, this, onClickDelete)
        mRecyclerView!!.adapter = mAdapter
        mAdapter!!.registerAdapterDataObserver(RecyclerViewEmptyObserver(mRecyclerView!!, statsEmptyView))
    }

    var onClickDelete = object : OnItemClicked {
        override fun onItemClick(position: Int) {
            val type = myDataset!!.keys.toTypedArray()[position]
            Log.d("canvas1234", "   on item click : type $type")
            statisticsPresenter!!.deleteAllByShape(type)
            makeText(this@StatisticsActivity, " All " + type + "s deleted. ")
            refreshData()
        }
    }

    /*
    Reload data in list, if coming from delete or any other action
     */
    private fun refreshData() {
        myDataset = statisticsPresenter!!.countByGroup as HashMap<Shape.Type, Int>
        mAdapter!!.setmDataSet(myDataset)
        mAdapter!!.notifyDataSetChanged()
    }
}