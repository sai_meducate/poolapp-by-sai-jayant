package Jayant.android.com.collision_detector.view

import Jayant.android.com.collision_detector.R
import Jayant.android.com.collision_detector.model.Shape
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.*

/**
 * Created by SaiJayant's Macbook on 06,July,2021
 */
class StatsAdapter(myDataset: HashMap<Shape.Type, Int>?, context: StatisticsActivity, onClick: OnItemClicked) : RecyclerView.Adapter<StatsAdapter.ViewHolder>() {
    private var mDataSet: HashMap<Shape.Type, Int>?
    private val mContext: Context
    private val onClick: OnItemClicked

    interface OnItemClicked {
        fun onItemClick(position: Int)
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val mDeleteButton: ImageButton
        var mTextView: TextView

        init {
            mTextView = v.findViewById<View>(R.id.textViewStats) as TextView
            mDeleteButton = v.findViewById<View>(R.id.deleteShape) as ImageButton
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): ViewHolder {
        // create a new view
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_stats_content, parent, false) as RelativeLayout
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val type = mDataSet!!.keys.toTypedArray()[position]
        val stats = " Shape : " + type + "  Count : " + mDataSet!![type]
        holder.mTextView.text = stats
        Log.d("canvas1234", " stats = " + stats + " pos= " + (position - 1))
        holder.mDeleteButton.setOnClickListener { onClick.onItemClick(holder.adapterPosition) }
    }

    override fun getItemCount(): Int {
        return if (mDataSet == null) 0 else mDataSet!!.size
    }

    fun setmDataSet(mDataSet: HashMap<Shape.Type, Int>?) {
        this.mDataSet = mDataSet
    }

    init {
        mContext = context
        mDataSet = myDataset
        this.onClick = onClick
    }
}