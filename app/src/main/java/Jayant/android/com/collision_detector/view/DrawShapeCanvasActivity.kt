package Jayant.android.com.collision_detector.view

import Jayant.android.com.collision_detector.R
import Jayant.android.com.collision_detector.model.Shape
import Jayant.android.com.collision_detector.presenter.CanvasPresenter
import Jayant.android.com.collision_detector.util.Constants.RADIUS
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * Created by SaiJayant's Macbook on 06,July,2021
 */
class DrawShapeCanvasActivity : AppCompatActivity() {
    private var canvas: CustomCanvas? = null
    var canvasPresenter: CanvasPresenter? = null
    private var maxY = 800 // average screen height
    private var maxX = 600 //average screen height

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_draw_canvas)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        canvas = findViewById<View>(R.id.canvasDrawView) as CustomCanvas
        canvasPresenter = CanvasPresenter(canvas!!, this)
        setupActionButtons()
        canvasWidthAndHeight
    }

    private fun setupActionButtons() {

        val fabCircle = findViewById<View>(R.id.fabCircle) as FloatingActionButton
        fabCircle.setOnClickListener { canvasPresenter!!.addShapeRandom(Shape.Type.CIRCLE) }
        val fabRect = findViewById<View>(R.id.fabRect) as FloatingActionButton
        fabRect.setOnClickListener { canvasPresenter!!.addShapeRandom(Shape.Type.SQUARE) }
        val fabTriangle = findViewById<View>(R.id.fabTriangle) as FloatingActionButton
        fabTriangle.setOnClickListener { canvasPresenter!!.addShapeRandom(Shape.Type.TRIANGLE) }
        val fabUndo = findViewById<View>(R.id.fabUndo) as FloatingActionButton
        fabUndo.setOnClickListener { canvasPresenter!!.undo()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_canvas_draw, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_stats) {
            startStatsView()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun startStatsView() {
        val intent = Intent(this, StatisticsActivity::class.java)
        startActivity(intent)
    }

    //Reduce radius so that shape isn't left incomplete at the edge
    private val canvasWidthAndHeight: Unit
        private get() {
            val viewTreeObserver = canvas!!.viewTreeObserver
            if (viewTreeObserver.isAlive) {
                viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        canvas!!.viewTreeObserver.removeOnGlobalLayoutListener(this)
                        maxY = canvas!!.height
                        maxX = canvas!!.width
                        //Reduce radius so that shape isn't left incomplete at the edge
                        canvasPresenter!!.setMaxX(maxX - RADIUS)
                        val bottomButtonHeight = 100
                        canvasPresenter!!.setMaxY(maxY - RADIUS - bottomButtonHeight)
                        removeOnGlobalLayoutListener(canvas, this)
                        Log.d(TAG, " Screen max x= $maxX maxy = $maxY")
                    }
                })
            }
        }

    companion object {
        private val TAG = DrawShapeCanvasActivity::class.java.simpleName

        /*Since global layout listener is called multiple times, remove it once we get the screen width and height
     */
        fun removeOnGlobalLayoutListener(v: View?, listener: OnGlobalLayoutListener?) {
            v!!.viewTreeObserver.removeOnGlobalLayoutListener(listener)
        }
    }
}