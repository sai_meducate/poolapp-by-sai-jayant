package Jayant.android.com.collision_detector.view

import Jayant.android.com.collision_detector.model.Shape
import Jayant.android.com.collision_detector.util.Constants
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import java.util.*

/**
 * Created by SaiJayant's Macbook on 06,July,2021
 */
class CustomCanvas(context: Context?, attrs: AttributeSet?) : View(context, attrs) {
    private val TAG = CustomCanvas::class.java.simpleName
    val RADIUS = Constants.RADIUS
    private var canvas: Canvas? = null
    var historyList: List<Shape> = ArrayList()
    private var longPressDone = false

    // defines paint and canvas
    private var drawPaint: Paint? = null

    // Setup paint with color and stroke styles
    private fun setupPaint() {
        drawPaint = Paint()
        drawPaint!!.color = Color.BLUE
        drawPaint!!.isAntiAlias = true
        drawPaint!!.strokeWidth = 5f
        drawPaint!!.style = Paint.Style.FILL_AND_STROKE
        drawPaint!!.strokeJoin = Paint.Join.ROUND
        drawPaint!!.strokeCap = Paint.Cap.ROUND
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        this.canvas = canvas
        Log.d(TAG, "  onDraw called")
        for (shape in historyList) {
            if (shape.isVisible) {
                when (shape.type) {
                    Shape.Type.CIRCLE -> {
                        drawPaint!!.color = Color.BLUE
                        canvas.drawCircle(shape.getxCordinate().toFloat(), shape.getyCordinate().toFloat(), RADIUS.toFloat(), drawPaint)
                        Log.d(TAG, "onDraw: circle - x " + shape.getxCordinate() + " y " + shape.getyCordinate())
                    }
                    Shape.Type.SQUARE -> {
                        drawRectangle(shape.getxCordinate(), shape.getyCordinate())
                        Log.d(TAG, "onDraw: squire - x " + shape.getxCordinate() + " y " + shape.getyCordinate())
                    }
                    Shape.Type.TRIANGLE -> {
                        drawTriangle(shape.getxCordinate(), shape.getyCordinate(), (2 * RADIUS))
                        Log.d(TAG, "onDraw: tringle - x " + shape.getxCordinate() + " y " + shape.getyCordinate())
                    }
                }
            }
        }
    }


    var squareSideHalf = 1 / Math.sqrt(2.0)

    //Consider pivot x,y as centroid.
    fun drawRectangle(x: Int, y: Int) {
        drawPaint!!.color = Color.RED
        val rectangle = Rect((x - squareSideHalf * RADIUS).toInt(), (y - squareSideHalf * RADIUS).toInt(), (x + squareSideHalf * RADIUS).toInt(), (y + squareSideHalf * RADIUS).toInt())
        canvas!!.drawRect(rectangle, drawPaint)
    }

    /*
    select three vertices of triangle. Draw 3 lines between them to form a traingle
     */
    fun drawTriangle(x: Int, y: Int, width: Int) {
        drawPaint!!.color = Color.GREEN
        val halfWidth = width / 2
        val path = Path()
        path.moveTo(x.toFloat(), (y - halfWidth).toFloat()) // Top
        path.lineTo((x - halfWidth).toFloat(), (y + halfWidth).toFloat()) // Bottom left
        path.lineTo((x + halfWidth).toFloat(), (y + halfWidth).toFloat()) // Bottom right
        path.lineTo(x.toFloat(), (y - halfWidth).toFloat()) // Back to Top
        path.close()
        canvas!!.drawPath(path, drawPaint)
    }

    init {
        isFocusable = true
        isFocusableInTouchMode = true
        setupPaint()
        Log.d(TAG, "  constructor called")
    }
}