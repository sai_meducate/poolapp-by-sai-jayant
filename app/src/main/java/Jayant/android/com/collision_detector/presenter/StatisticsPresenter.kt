package Jayant.android.com.collision_detector.presenter

import Jayant.android.com.collision_detector.interactor.ShapesInteractor
import Jayant.android.com.collision_detector.model.Shape
import java.io.Serializable

/**
 * Created by SaiJayant's Macbook on 06,July,2021
 */
/*
    Bind Stats activity with interactor
 */
class StatisticsPresenter {

    val countByGroup: Serializable
        get() = ShapesInteractor.instance.countByGroup

    fun deleteAllByShape(shapeType: Shape.Type) {
        ShapesInteractor.instance.deleteAllByShape(shapeType)
    }
}
